let nb_caneles = 0;
let nb_fours = 0;

function cuisiner(){
    nb_caneles += 1;
}

function afficherCaneles(){
    //Affiche le nombre de canelés dans la page
    document.getElementById('nb').innerHTML = nb_caneles;
}

function click(){
    cuisiner();
    afficherCaneles()
}

function acheterFour(){
    if (nb_caneles >= 100){
        nb_caneles -= 100;
        nb_fours += 1;
    }
}

function afficherFour(){
    document.getElementById('four').innerHTML = nb_fours;
}

function clickFour(){
    acheterFour();
    afficherFour();
    afficherCaneles();
}

var btn = document.getElementById('bout');
btn.addEventListener('click', click)

var btn2 = document.getElementById('btn2');
btn2.addEventListener('click', clickFour);